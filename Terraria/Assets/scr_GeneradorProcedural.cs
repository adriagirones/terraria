﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_GeneradorProcedural : MonoBehaviour
{
    public GameObject[] squares;
    private GameObject actualSquare;

    public GameObject[] minerals;

    public GameObject torch;

    public int seed = 0;
    public int seedMineral;
    public int seedTorch;

    public float amp = 100f;
    public float freq = 10f;
    public float freq2 = 10f;
    public float freq3 = 10f;

    void Start()
    {
        Vector2 pos = this.transform.position;

        int col = 100;
        int fil = 100;
        for (int x = 0; x < col; x++)
        {
            for (int y = 0; y < fil; y++)
            {
                if(x == 0 || y == 0 || x == col-1 || y == fil - 1)
                {
                    actualSquare = squares[1];
                }
                else
                {
                    float probBlock = Mathf.PerlinNoise((pos.x + x + seed) / freq, (pos.y + y + seed) / freq);
                    probBlock = probBlock * amp;


                    if (probBlock > 70 * (amp / 100))
                    {
                        actualSquare = squares[0];
                    }
                    else if (probBlock > 40 * (amp / 100))
                    {
                        actualSquare = squares[1];
                    }
                    else
                    {
                        actualSquare = squares[2];
                        float probTorch = Mathf.PerlinNoise((pos.x + x + seedTorch) / freq2, (pos.y + y + seedTorch) / freq2);
                       // print(probTorch);

                        if (probTorch > 0.8f)
                        {
                            GameObject newTorch = Instantiate(torch);
                            newTorch.transform.position = new Vector2(pos.x + x, pos.y + y);
                        }
                        else
                        {
                            float probMineral = Mathf.PerlinNoise((pos.x + x + seedMineral) / freq3, (pos.y + y + seedMineral) / freq3);
                            print(probMineral);
                            if (probMineral > 0.7f)
                            {
                                GameObject newMineral = Instantiate(minerals[Random.Range(0, minerals.Length)]);
                                newMineral.transform.position = new Vector2(pos.x + x, pos.y + y);
                            }
                        }

                    }
                }

                GameObject newBlock = Instantiate(actualSquare);
                newBlock.transform.position = new Vector2(pos.x + x, pos.y + y);

            }
        }
    }


}
